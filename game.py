from random import randint

#getting the name
greetings = input('Hi! What is your name? ').strip().capitalize()

#Declaring x for my guess number
x = 1

months = {
        1:'January ',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December'
  }
def later_or_earlier(a, b, c, d):
    month = a
    year = b
    count = c
    while True:
        if d == 'later':
            number = randint(month, 12)
            dict_month = months[number]
            guess_year = randint(year, 2004)
            random_guess = f'{dict_month} / {guess_year}'
        elif d == 'earlier':
            number = randint(1, month)
            dict_month = months[number]
            guess_year = randint(1924, year)
            random_guess = f'{dict_month} / {guess_year}'
        print(f'Guess {count}: {greetings} were you born in {random_guess} ?')
        yes_or_no = input('yes or no? ')
        if yes_or_no == 'yes':
            print('I knew it!')
            exit()
        elif count == 5:
            print('I have other things to do. Good bye.')
            exit()
        elif yes_or_no == 'later' or yes_or_no == 'earlier':
            count+=1
            later_or_earlier(guess_month, guess_year, count, yes_or_no)
        elif yes_or_no == 'no':
            print('Drat! Lemme try again!')
            count+=1

for i in range(6):

    guess_month = randint(1, 12)
    guess_year = randint(1924, 2004)
    random_guess = f'{guess_month} / {guess_year}'
    print(f'Guess {x}: {greetings} were you born in {random_guess} ?')
    yes_or_no = input('yes or no? ')
    if yes_or_no == 'yes':
        print('I knew it!')
        exit()
    elif x == 5:
        print('I have other things to do. Good bye.')
        exit()
    elif yes_or_no == 'later' or yes_or_no == 'earlier':
        x+=1
        later_or_earlier(guess_month, guess_year, x, yes_or_no)
    elif yes_or_no == 'no':
        print('Drat! Lemme try again!')
        x+=1
